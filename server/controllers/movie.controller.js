import {request,response} from 'express';

import * as Validators from './../utils/validators';
import {pick} from './../utils/utils';
import Movie from '../models/Movie.model';
import { findByIdOrFail } from '../middlewares/user.middleware';

export const getAllMovies = async(req = request, res = response)=>{
    const movies = await Movie.find();
	res.send(movies);
}

export const getMovieById = async(req = request, res = response , next)=>{ 
    try {
        const { id } = req.params;
        const movie = await findByIdOrFail(id);
        res.send(movie); 
    } catch (error) {
        next(error);
    }
}

export const saveMovie = async(req = request, res = response, next)=>{
    try {
        const { name, release_date, avg_rating } = req.body;
        var newMovie = {name, release_date, avg_rating}
        Validators.verifyEmpty(newMovie);
        newMovie = Movie(req.body);  
        newMovie = await newMovie.save();

        res.status(201).send({
            success: true,
            message: 'Pelicula creada correctamente',
            data: newMovie
        });
    } catch (error) {
        next(error);
    }
}

export const updateMovie = async(req = request, res = response, next)=>{
    try {
        const { id } = req.params;
        var body = pick(req.body,['name','release_date','avg_rating']);
        Validators.verifyEmpty(body); 
        const newMovie = await Movie.findByIdAndUpdate(id,body);
        res.send({
            success: true,
            message: 'Pelicula actualizada correctamente'
        });
    } catch (error) {
        next(error);
    }
}

export const deleteMovie = async(req = request, res = response, next)=>{
    try {
        const { id } = req.params;  
        var movie = await findByIdOrFail(id);
        movie = await movie.delete();

        res.send({
            success: true,
            message: 'Pelicula eliminada correctamente',
            data: movie
        });
    } catch (error) {
        next(error);
    }
}