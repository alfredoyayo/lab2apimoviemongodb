import mongoose from 'mongoose';

const dbConnection = async () => {
	try {
		await mongoose.connect(process.env.DB_CONNECTION, {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useCreateIndex: true,
			useFindAndModify: false
		});
		console.log('Database successfull connected!');
	} catch (error) {
		console.error(error);
		throw new Error('Error en la base de tados, porfavor comuníquese con el administrador!');		
	};
};

export default dbConnection;