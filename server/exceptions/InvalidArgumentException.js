export default class InvalidArgumentException extends Error {
  constructor(message = "", ...args) {
    super(message, ...args);
  }
}