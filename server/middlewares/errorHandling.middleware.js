import InvalidArgumentException from '../exceptions/InvalidArgumentException';
import {Error} from 'mongoose';

const errorHandling =(error, req, res, next) => { 
    if (error instanceof InvalidArgumentException || error instanceof Error) {
        res.status(400).send({
            succes:false,
            error:error.message
        });
    } else {
        res.status(500).send({
            error:error.message,
            succes:false
        });
    }
}

module.exports = errorHandling;