import InvalidArgumentException from '../exceptions/InvalidArgumentException';
import Movie from './../models/Movie.model';

export const findByIdOrFail= async (id)=>{ 
    try {
        const movie = await Movie.findById(id); 
        if(!movie){throw new Error();}
        return movie;
    } catch (error) { 
        throw new InvalidArgumentException(`no se pudo encontrar una pelicula con el id: ${id}`);
    } 
}