import { Schema, model } from 'mongoose';

const MovieSchema = Schema({
	name: {
		type: String,
        required : [true , 'El name es requerido'],
	},
	release_date: {
		type: Date,
		required : [true , 'El release_date es requerido'],
	},
	avg_rating: {
		type : Number,
		required: [true , 'El avg_rating es requerido'],
	}
});

MovieSchema.methods.toJSON = function(){
    let movie = this;
    let movieObject = movie.toObject();
    delete movieObject.__v ;
    return movieObject;
}

export default model('movies', MovieSchema);