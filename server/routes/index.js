const express = require('express');
const app = express();
const errorHandling = require('../middlewares/errorHandling.middleware');
import  moviesRoutes from "./movie.routes";

app.use(express.json());
app.use('/movies',moviesRoutes);
app.use(errorHandling);

export default app;