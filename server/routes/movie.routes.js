import {Router} from 'express';
import errorHandling from '../middlewares/errorHandling.middleware';

import {getAllMovies,getMovieById,saveMovie,deleteMovie,updateMovie} from './../controllers/movie.controller';

const router = Router();

router.get('/',getAllMovies);
router.get('/:id',getMovieById);
router.post('/',saveMovie);
router.patch('/:id',updateMovie);
router.delete('/:id',deleteMovie);

export default router;