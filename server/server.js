import express from 'express';
import dotenv from 'dotenv';

import dbConnection from './database/config';
import routes from './routes';

dotenv.config();
const app = express();
const server = require('http').createServer(app);
dbConnection();
 
app.use(routes);  

server.listen(process.env.PORT,()=>{
    console.log(`corriendo en el puerto ${process.env.PORT}`)
});