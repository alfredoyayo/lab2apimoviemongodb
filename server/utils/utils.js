export const pick = (object,keys)=>{
    const _object = {};
    for (const key of keys) {
        if(key in object){
            _object[key]=object[key]
        }
    }
    return _object;
}  