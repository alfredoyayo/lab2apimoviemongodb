import InvalidArgumentException from './../exceptions/InvalidArgumentException';

const verifyEmpty = (object)=>{ 
    for (const key in object) {
        const value=object[key];
        if(!value){
            throw new InvalidArgumentException(`El campo ${key} es requerido o vacío`);
            }
        
        if(typeof(value)==="string"&&value.length<=0){
            throw new InvalidArgumentException(`El campo ${key} no debe estar vacío`);
        }
    } 
    return object;
}

export const verifyIsNumber = (value)=>{ 
    if(!(!isNaN(parseFloat(value)) && isFinite(value))){
        throw new InvalidArgumentException(`${value} no es un número`);
    } 
}

const verifyEmail = (value)=>{
    verifyEmpty(value,"El campo email esta vacío");
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regex.test(String(value).toLowerCase())){
        throw new InvalidArgumentException("el email no es válido");
    }
    return value;
}

export {
    verifyEmail,
    verifyEmpty
}